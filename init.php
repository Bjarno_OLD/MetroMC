<?php

namespace metromc;

error_reporting(E_ALL);
ini_set("display_errors", "1");

require_once("classes/reader.php");
require_once("classes/stop.php");
require_once("classes/operator.php");
require_once("classes/line.php");
require_once("classes/selfLine.php");
require_once("classes/route.php");
require_once("classes/planner.php");
require_once("classes/transportType.php");
require_once("classes/stopGroup.php");

require_once("libraries/astar.php");
require_once("libraries/json_minify.php");

require_once("functions.php");

$settings = reader::settingsReader("data/settings.json");

reader::operatorReader("data/operators.json");

operator::setDefaultOperator(
	operator::getOperator($settings->defaultOperatorId)
);

reader::transportTypesReader("data/transportTypes.json");
reader::stopsReader("data/stops.json");
reader::linesReader("data/lines_metro_breeg.json");
reader::linesReader("data/lines_metro_shared.json");
reader::linesReader("data/lines_metro_tembie.json");
reader::linesReader("data/lines_train.json");
reader::stopGroupsReader("data/stopGroups.json");

planner::setTransferCost($settings->transferCost);
stopGroup::setExtraCost($settings->groupTransferCost);

$loadTime = (microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]) * 1000;