<?php

namespace metromc;

head("Lijnoverzicht");
?>

<h1>Lijnoverzicht</h1>

<p>De volgende lijnen zijn bekend.</p>

<ul>
<?php

$lines = line::getLines();

usort($lines, function ($a, $b) { return $a->getSort() - $b->getSort(); });

foreach ($lines as $line) {
	echo("<li><a href=\"?page=line&lineid=" . $line->getLineId() . "\">" . $line->getName() . "</a></li>");
}

?>
</ul>

<?php
foot();
?>