<?php

namespace metromc;

if (!(isset($_GET["operatorid"]))) {
	die("operatorid required");
}

$operatorId = $_GET["operatorid"];
$operator = operator::getOperator($operatorId);

if ($operator === NULL) {
	error404();
}

head($operator->getName());
?>

<h1><?= $operator->getName(); ?></h1>

<?php
foot();
?>