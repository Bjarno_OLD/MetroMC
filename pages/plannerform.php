<?php

namespace metromc;

head("Routeplanner");

$stops = stop::getStops();
asort($stops);

$transportTypes = transportType::getTransportTypes();
asort($transportTypes);

?>

<h1>Routeplanner</h1>

<form method="get" action=".">

<input type="hidden" name="page" value="planner">

<table>
<tr><td>Vertrek:</td><td>
	<select name="vertrek">
		<?php
		foreach ($stops as $stop) {
			echo("\t<option value=\"" . $stop->getStopId() . "\">" . $stop->getName() . "</option>");
		}
		?>
	</select>
</td></tr>
<tr><td>Aankomst:</td><td>
	<select name="aankomst">
		<?php
		foreach ($stops as $stop) {
			echo("\t<option value=\"" . $stop->getStopId() . "\">" . $stop->getName() . "</option>");
		}
		?>
	</select>
</td></tr>
<tr><td>Transporttypes:</td><td>
	<?php
	foreach ($transportTypes as $type) {
		echo("<input type=\"checkbox\" name=\"types[]\" value=\"" . $type->getTransportTypeId());
		echo("\" checked=\"checked\"> " . $type->getName() . "<br>");

	}
	?>
</td></tr>
<tr><td></td><td><input type="submit" value="Bereken route">
</td></tr>
</table>

</form>

<?php
foot();
?>