<?php

namespace metromc;

if (!(isset($_GET["lineid"]))) {
	die("lineid required");
}

$lineId = $_GET["lineid"];
$line = line::getLine($lineId);

if ($line === NULL) {
	error404();
}

head($line->getName());

?>

<h1><?= $line->getName(); ?></h1>

<p>Deze lijn wordt uitgebaat door
<?php

$operators = $line->getOperators();
$size = count($operators);

if ($size == 0) {
	echo("geen enkele operator");
} else {
	echo("<a href=\"?page=operator&operatorid=" . $operators[0]->getOperatorId() . "\">" . $operators[0]->getName() . "</a>");
	for ($i = 1; $i < $size; $i++) {
		if ($i == $size - 1) {
			echo (" en ");
		} else {
			echo(", ");
		}
		echo("<a href=\"?page=operator&operatorid=" . $operators[$i]->getOperatorId() . "\">" . $operators[$i]->getName() . "</a>");
	}
}

?>
.</p>

<?php if ($line->getType() == line::TYPE_SELF) { ?>

<h2>Routes</h2>

<?php
$routes = $line->getRoutes();

foreach ($routes as $route) {
	$start = $route->getStart();
	?>
	<h3><?= $route->getName(); ?></h3>
	
	<ul>
	<?php
	echo("<li><a href=\"?page=stop&stopid=" . $start->getStopId() . "\">" . $start->getName() . "</a></li>");

	$size = $route->getSize();
	for ($i = 1; $i < $size; $i++) {
		$time = $route->getNodeTime($i);
		$stop = $route->getNodeStop($i);
		echo("<li><a href=\"?page=stop&stopid=" . $stop->getStopId() . "\">" . $stop->getName() . "</a> (+{$time}s)</li>");
	}
	?>
	</ul>

	<?php
}
?>

<?php } ?>