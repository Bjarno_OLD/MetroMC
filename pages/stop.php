<?php

namespace metromc;

if (!(isset($_GET["stopid"]))) {
	die("stopid required");
}

$stopId = $_GET["stopid"];
$stop = stop::getStop($stopId);

if ($stop == NULL) {
	error404();
}

head($stop->getName());
?>

<h1><?= $stop->getName(); ?></h1>

<p>Deze halte wordt uitgebaat door <?= $stop->getOperator()->getName(); ?>.</p>

<?php
if ($stop->isClosed()) {
?><p>Deze halte is momenteel gesloten, er is geen toegang mogelijk en er stoppen geen voertuigen.</p><?php
}
?>

<?php
$x = $stop->getData("x");
$y = $stop->getData("y");
$z = $stop->getData("z");

if ($x !== NULL) {
	echo("<iframe src=\"https://mirkwood.bjarno.be/mc/maps/#maponepointeight/0/10/{$x}/{$z}/{$y}\" style=\"width: 100%; height: 375px;\"></iframe>");
	echo("<p><a href=\"https://mirkwood.bjarno.be/mc/maps/#maponepointeight/0/10/{$x}/{$z}/{$y}\" target=\"_blank\">Vergroten</a></p>");
}

?>

<h2>Lijnen die langs deze halte komen</h2>

<?php
$lines = $stop->getLinesStopping();

if (count($lines) == 0) {
?><p>Er stoppen geen lijnen bij deze halte.</p><?php
} else {

	echo("<ul>");
	foreach ($lines as $line) {
		echo("<li><a href=\"?page=line&lineid=" . $line->getLineId() . "\">" . $line->getName() . "</a></li>");
	}
	echo("</ul>");
}

?>

<?php
foot();
?>