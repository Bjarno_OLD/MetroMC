<?php

namespace metromc;

if (!( (isset($_GET["vertrek"])) && (isset($_GET["aankomst"])) )) {
	include("pages/plannerform.php");
	die();
}

$fromId = $_GET["vertrek"];
$toId = $_GET["aankomst"];

if (isset($_GET["types"])) {
	$transportTypeIds = $_GET["types"];
	$transportTypes = array();

	foreach ($transportTypeIds as $transportTypeId) {
		$transportTypes[] = transportType::getTransportType($transportTypeId);
	}
} else {
	$transportTypes = transportType::getTransportTypes();
}

$from = stop::getStop($fromId);
$to = stop::getStop($toId);

if ($from == $to) {
	die("Deperature stop equals destination stop...");
}

if ($from === NULL) {
	die("Departure does not exists.");
}

if ($to === NULL) {
	die("Destination does not exists.");
}

head("Reis van " . $from->getName() . " naar " . $to->getName());
?>

<h1>Reis van <?= $from->getName(); ?> naar <?= $to->getName(); ?></h1>

<?php
$startTime = microtime(true);
$result = planner::searchPath($from, $to, $transportTypes);

if ($result == false) {
	echo("<p>We hebben alles geprobeerd, maar we konden geen manier vinden om je bestemming te bereiken.</p>");
} else {
	$nodes = planner::nodeToListOfNodes($result);
	$size = count($nodes);

	$numberOfTransfers = 0;
	$operators = array();
	$disruption = false;

	echo("<ul>");

	for ($i = 0; $i < $size; $i++) {
		$node = $nodes[$i];

		$showTransportType = false;

		echo("<li>");

		if ($i == 0) {
			$nextNode = $nodes[$i + 1];
			$route = $nextNode->getState()->getRoute();

			$routeSize = $route->getSize();
			$lastNodeOfRoute = $route->getNodeStop($routeSize - 1);

			$stop = $node->getState()->getStop();

			if ($nextNode->getState()->hasPassBy()) {
				$stop = $nextNode->getState()->getPassBy();
			}

			echo("Vertrek in halte <a href=\"?page=stop&stopid=" . $stop->getStopId() . "\">");
			echo($stop->getName() . "</a> op <a href=\"?page=line&lineid=" . $route->getLine()->getLineId());
			echo("\">" . $route->getLine()->getName() . "</a> richting " . $lastNodeOfRoute->getName() . ".");

			$showTransportType = true;

			$oldRoute = $route;
		} elseif ($i == $size - 1) {
			echo("Aankomst in <a href=\"?page=stop&stopid=" . $node->getState()->getStop()->getStopId() . "\">");
			echo($node->getState()->getStop()->getName() . "</a>.");
		} else {
			$nextNode = $nodes[$i + 1];
			$route = $nextNode->getState()->getRoute();

			// Transfer if next node uses a different route
			if ($oldRoute != $route) {

				$routeSize = $route->getSize();
				$lastNodeOfRoute = $route->getNodeStop($routeSize - 1);

				$transferStop = $node->getState()->getStop();

				// Transfer from station in same group
				if ($nextNode->getState()->hasPassBy()) {
					$transferStop = $nextNode->getState()->getPassBy();

					echo("Stap uit in halte <a href=\"?page=stop&stopid=" . $node->getState()->getStop()->getStopId() . "\">");
					echo($node->getState()->getStop()->getName() . "</a>. Vervolg je reis in ");
				} else {
					echo("Stap over in ");
				}

				
				echo("<a href=\"?page=stop&stopid=" . $transferStop->getStopId() . "\">");
				echo($transferStop->getName() . "</a> op <a href=\"?page=line&lineid=" . $route->getLine()->getLineId());
				echo("\">" . $route->getLine()->getName() . "</a> richting " . $lastNodeOfRoute->getName() . "");
				$oldRoute = $route;

				$numberOfTransfers++;

				$showTransportType = true;
			} else {
				echo("Passeer halte <a href=\"?page=stop&stopid=" . $node->getState()->getStop()->getStopId() . "\">");
				echo($node->getState()->getStop()->getName() . "</a>.");
			}
		}

		if ($showTransportType) {
			echo(" (" . $nextNode->getState()->getRoute()->getLine()->getTransportType()->getName()  . ")");
		}

		echo("</li>" . PHP_EOL . PHP_EOL . "\t\t");

		$operator = $node->getState()->getStop()->getOperator();
		if (!(in_array($operator, $operators))) {
			$operators[] = $operator;
		}

		if ($node->getState()->getStop()->isClosed()) {
			$disruption = true;
		}
	}

	$lastNode = $nodes[$size - 1];

	

	echo("</ul>");

	echo("<p>Totale tijd: " . gmdate("i:s", $lastNode->getTotalCost()) . ". " . $numberOfTransfers . "&times; overstappen.</p>");

	if ($disruption) {
		echo("<p>Eén of meerdere stations zijn gesloten, het is mogelijk dat deze reis niet mogelijk is, of bepaalde gevens foutief zijn.</p>");
	}

	echo("<p>De volgende operators zijn verantwoordelijk om je een snelle reis te bezorgen:</p>");

	echo("<ul>");
	foreach ($operators as $operator) {
		echo("<li><a href=\"?page=operator&operatorid=" . $operator->getOperatorId() . "\">" . $operator->getName() . "</a></li>");
	}
	echo("</ul>");

	echo("<hr>");
	$endTime = microtime(true);
	echo("<p>It took " . round(($endTime - $startTime) * 1000, 4) . " ms to generate this route.</p>");
}

?>

<?php
foot();
?>