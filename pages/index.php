<?php

namespace metromc;

head("MetroMC");
?>

<h1>MetroMC</h1>

<ul>
<li><a href="?page=lines">Bekijk lijnoverzicht</a></li>
<li><a href="?page=planner">Routeplanner</a></li>
<li><a href="?page=netmap">Bekijk de netkaart</a></li>
</ul>

<hr>

<p>Ik ben open source: <a href="https://gitlab.com/Bjarno/MetroMC/" target="_blank">Bekijk mij op GitLab</a>!</p>

<?php
foot();
?>