<?php

namespace metromc;

class reader {

	public static function operatorReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$operators = json_decode($json);

		foreach ($operators as $operator) {
			$operatorId = $operator->operatorId;
			$name = $operator->name;

			new operator($operatorId, $name);
		}
	}

	public static function stopsReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$stops = json_decode($json);

		foreach ($stops as $stop) {
			$stopId = $stop->stopId;
			$name = $stop->name;
			$operatorId = NULL;
			$isClosed = false;
			$data = array();

			if (isset($stop->operatorId)) {
				$operatorId = $stop->operatorId;
			}

			if (isset($stop->isClosed)) {
				$isClosed = $stop->isClosed;
			}

			if (isset($stop->data)) {
				$data = (array) $stop->data;
			}

			$operator = operator::getOperator($operatorId);

			if ($operator === NULL) {
				$operator = operator::getDefaultOperator();
			}

			new stop($stopId, $name, $isClosed, $operator, $data);
		}
	}

	public static function settingsReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$settings = json_decode($json);

		return $settings;
	}

	public static function linesReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$lines = json_decode($json);

		foreach ($lines as $line) {
			$lineId = $line->lineId;
			$name = $line->name;
			$lineType = $line->lineType;
			$operatorIds = $line->operatorIds;
			$sortIdx = $line->sort;
			$transportTypeId = $line->transportTypeId;

			$transportType = transportType::getTransportType($transportTypeId);

			switch ($lineType) {
				case "self":
					$obj = new selfLine($lineId, $name, $transportType, $sortIdx);
					self::setSelfLine($obj, $line);
					break;
			}

			foreach ($operatorIds as $id) {
				$operator = operator::getOperator($id);
				$obj->addOperator($operator);
			}
		}
	}

	private static function setSelfLine($obj, $data) {
		$routes = $data->routes;

		foreach ($routes as $route) {
			self::setSelfLineRoute($obj, $data, $route);
		}
	}

	private static function setSelfLineRoute($obj, $data, $route) {
		$start = stop::getStop($route->start);
		$end = stop::getStop($route->end);
		$stops = $route->stops;
		$travelTimes = $route->travelTimes;
		$routeId = $route->routeId;
		$name = $route->name;

		$route = $obj->addRoute($routeId, $name, $start);
		$size = count($stops);

		for ($i = 0; $i < $size; $i++) {
			$stopname = $stops[$i];
			$stop = stop::getStop($stopname);
			$time = $travelTimes[$i];
			$route->addNode($stop, $time);

			if (!$stop) {
				die("stop \"$stopname\" not found!");
			}

			$stop->addLineStopping($obj);
		}

		// Add final node
		$route->addNode($end, $travelTimes[$size]);

		// Add start and final node to stop
		$start->addLineStopping($obj);
		$end->addLineStopping($obj);
	}

	public static function transportTypesReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$transportTypes = json_decode($json);

		foreach ($transportTypes as $transportType) {
			$transportTypeId = $transportType->transportTypeId;
			$name = $transportType->name;

			new transportType($transportTypeId, $name);
		}
	}

	public static function stopGroupsReader($filename) {
		$json = file_get_contents($filename);
		$json = json_minify($json);
		$stopGroups = json_decode($json);

		foreach ($stopGroups as $stopGroup) {
			$stopGroupId = $stopGroup->stopGroupId;
			$stopIds = $stopGroup->stopIds;

			$group = new stopGroup($stopGroupId);

			foreach ($stopIds as $stopId) {
				$stop = stop::getStop($stopId);
				$group->addStop($stop);
			}
		}
	}
}