<?php

namespace metromc;

class transportType {

	private $transportTypeId;
	private $name;

	private static $transportTypes = array();

	public static function getTransportType($transportTypeId) {
		if (isset(self::$transportTypes[$transportTypeId])) {
			return self::$transportTypes[$transportTypeId];
		} else {
			return NULL;
		}
	}

	public static function getTransportTypes() {
		return self::$transportTypes;
	}

	public function __construct($transportTypeId, $name) {
		$this->transportTypeId = $transportTypeId;
		$this->name = $name;
		
		self::$transportTypes[$transportTypeId] = $this;
	}

	public function getTransportTypeId() {
		return $this->transportTypeId;
	}

	public function getName() {
		return $this->name;
	}

}