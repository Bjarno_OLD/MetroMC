<?php

namespace metromc;

class selfLine extends line {

	protected $routes = array();

	public function getType() {
		return line::TYPE_SELF;
	}

	public function __construct($lineId, $name, $transportType, $sortIdx) {
		parent::__construct($lineId, $name, $transportType, $sortIdx);
	}

	public function addRoute($routeId, $name, $start) {
		$route = new route($routeId, $name, $start, $this);
		$this->routes[$routeId] = $route;
		return $route;
	}

	public function getRoutes() {
		return $this->routes;
	}

	

}