<?php

namespace metromc;

class route {

	private $routeId;
	private $name;
	private $start = NULL;
	private $nodes = array();
	private $travelTimes = array();
	private $line;

	private static $routes = array();

	public function getRoute($routeId) {
		if (isset(self::$routes[$routeId])) {
			return self::$routes[$routeId];
		} else {
			return NULL;
		}
	}

	public function __construct($routeId, $name, $start, $line) {
		$this->routeId = $routeId;
		$this->name = $name;
		$this->start = $start;
		$this->line = $line;

		self::$routes[$routeId] = $this;
	}

	public function getLine() {
		return $this->line;
	}

	public function getRouteId() {
		return $this->routeId;
	}

	public function getName() {
		return $this->name;
	}

	public function addNode($stop, $time) {
		$this->nodes[] = $stop;
		$this->travelTimes[] = $time;
	}

	public function getSize() {
		return count($this->nodes) + 1;
	}

	public function getStart() {
		return $this->start;
	}

	public function getNodeStop($nodeIdx) {
		if ($nodeIdx == 0) {
			return $this->start;
		}
		$nodeIdx--;
		return $this->nodes[$nodeIdx];
	}

	public function getNodeTime($nodeIdx) {
		if ($nodeIdx == 0) {
			return 0;
		}
		$nodeIdx--;
		return $this->travelTimes[$nodeIdx];
	}

	public function getStopPositions($stop) {
		$result = array();

		if ($stop == $this->start) {
			$result[] = 0;
		}

		for ($i = 0; $i < count($this->nodes); $i++) {
			$node = $this->nodes[$i];

			if ($node == $stop) {
				$result[] = $i + 1;
			}
		}

		return $result;
	}

}