<?php

namespace metromc;

class stopGroup {

	private $stopGroupId;
	private $stops = array();

	private static $stopGroups = array();
	private static $extraCost = 20;

	public static function getStopGroup($stopGroupId) {
		if (isset(self::$stopGroups[$stopGroupId])) {
			return self::$stopGroups[$stopGroupId];
		} else {
			return NULL;
		}
	}

	public static function getStopGroups() {
		return self::$stopGroups;
	}

	public static function getExtraCost() {
		return self::$extraCost;
	}

	public static function setExtraCost($extraCost) {
		self::$extraCost = $extraCost;
	}

	public function __construct($stopGroupId) {
		$this->stopGroupId = $stopGroupId;

		self::$stopGroups[$stopGroupId] = array();
	}

	public function addStop($stop) {
		if ($stop->getStopGroup() === NULL) {
			$this->stops[] = $stop;
			$stop->setStopGroup($this);
		}
	}

	public function getDirectConnections($main = NULL) {
		$result = array();

		foreach ($this->stops as $stop) {
			$stopResults = $stop->getDirectConnections();

			foreach ($stopResults as $stopResult) {
				if ($stop == $main) {
					$result[] = array(
						"stop" => $stopResult["stop"],
						"route" => $stopResult["route"],
						"time" => $stopResult["time"],
						"passby" => $stop
					);
				} else {
					$result[] = array(
						"stop" => $stopResult["stop"],
						"route" => $stopResult["route"],
						"time" => $stopResult["time"] + self::$extraCost,
						"passby" => $stop
					);
				}
			}
		}

		return $result;
	}

	public function getStops() {
		return $this->stops;
	}

}