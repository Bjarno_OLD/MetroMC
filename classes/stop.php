<?php

namespace metromc;

class stop {
	private $stopId;
	private $name;
	private $isClosed;
	private $operator;
	private $data;

	private $linesStopping = array();
	private $stopGroup = NULL;

	private static $stops = array();

	public static function getStop($stopId) {
		if (isset(self::$stops[$stopId])) {
			return self::$stops[$stopId];
		} else {
			return NULL;
		}
	}

	public static function getStops() {
		return self::$stops;
	}

	public function __construct($stopId, $name, $isClosed = false, $operator = NULL, $data = array()) {
		$this->stopId = $stopId;
		$this->name = $name;
		$this->isClosed = $isClosed;
		$this->operator = $operator;
		$this->data = $data;

		self::$stops[$stopId] = $this;
	}

	public function getStopId() {
		return $this->stopId;
	}

	public function getName() {
		return $this->name;
	}

	public function isClosed() {
		return $this->isClosed;
	}

	public function hasTrainConnection() {
		return $this->hasTrainConnection;
	}

	public function getOperator() {
		return $this->operator;
	}

	public function addLineStopping($line) {
		if (!(in_array($line, $this->linesStopping))) {
			$this->linesStopping[] = $line;
		}
	}

	public function getLinesStopping() {
		return $this->linesStopping;
	}

	public function getData($name = NULL, $default = NULL) {
		if ($name === NULL) {
			return $this->data;
		} elseif (isset($this->data[$name])) {
			return $this->data[$name];
		} else {
			return $default;
		}
	}

	public function getDirectConnections() {
		$directionConnections = array();

		foreach ($this->linesStopping as $line) {
			switch ($line->getType()) {
				case line::TYPE_SELF:
					$routes = $line->getRoutes();
					foreach ($routes as $route) {
						$positions = $route->getStopPositions($this);

						foreach ($positions as $position) {
							$nextPosition = $position + 1;
							if ($nextPosition < $route->getSize()) {
								$directionConnections[] = [
									"stop" => $route->getNodeStop($nextPosition),
									"route" => $route,
									"time" => $route->getNodeTime($nextPosition)
								];
							}
						}
					}
					break;
			}
		}

		return $directionConnections;
	}

	public function getStopGroup() {
		return $this->stopGroup;
	}

	public function setStopGroup($stopGroup) {
		$this->stopGroup = $stopGroup;
	}
}