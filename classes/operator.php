<?php

namespace metromc;

class operator {
	private $operatorId;
	private $name;

	private static $operators = array();
	private static $defaultOperator = NULL;

	public static function getOperator($operatorId) {
		if (isset(self::$operators[$operatorId])) {
			return self::$operators[$operatorId];
		} else {
			return NULL;
		}
	}

	public static function getDefaultOperator() {
		return self::$defaultOperator;
	}

	public static function setDefaultOperator($operator) {
		self::$defaultOperator = $operator;
	}

	public function __construct($operatorId, $name) {
		$this->operatorId = $operatorId;
		$this->name = $name;

		self::$operators[$operatorId] = $this;

		if (count(self::$operators) == 1) {
			self::$defaultOperator = $this;
		}
	}

	public function getOperatorId() {
		return $this->operatorId;
	}

	public function getName() {
		return $this->name;
	}
}