<?php

namespace metromc;

abstract class line {

	protected $lineId;
	protected $name;
	protected $transportType;
	protected $operators = array();
	protected $sortIdx = 0;

	private static $lines;

	const TYPE_SELF = 1;

	public static function getLine($lineId) {
		if (isset(self::$lines[$lineId])) {
			return self::$lines[$lineId];
		} else {
			return NULL;
		}
	}

	public static function getLines() {
		return self::$lines;
	}

	public function getType() {
		return false;
	}

	protected function __construct($lineId, $name, $transportType, $sortIdx) {
		$this->lineId = $lineId;
		$this->name = $name;
		$this->transportType = $transportType;
		$this->sortIdx = $sortIdx;

		self::$lines[$lineId] = $this;
	}

	public function getSort() {
		return $this->sortIdx;
	}

	public function addOperator($operator) {
		$this->operators[] = $operator;
	}

	public function getOperators() {
		return $this->operators;
	}

	public function getLineId() {
		return $this->lineId;
	}

	public function getName() {
		return $this->name;
	}

	public function getTransportType() {
		return $this->transportType;
	}

}