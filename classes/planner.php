<?php

namespace metromc;

use \be\bjarno\astar\solver as astar;

class plannerState {
	
	private $stop;
	private $route;
	private $passby;

	public function __construct($stop, $route, $passby = false) {
		$this->stop = $stop;
		$this->route = $route;
		$this->passby = $passby;
	}

	public function getUniqueId() {
		if ($this->route === NULL) {
			return "initial";
		}
		return $this->stop->getStopId();
		// return $this->stop->getStopId() . $this->route->getLine()->getLineId() . $this->route->getRouteId();
	}

	public function getStop() {
		return $this->stop;
	}

	public function getRoute() {
		return $this->route;
	}

	public function hasPassBy() {
		return ($this->passby !== false);
	}

	public function getPassBy() {
		return $this->passby;
	}
}

class planner {

	private static $transferCost = 10;

	public static function settransferCost($transferCost) {
		self::$transferCost = $transferCost;
	}

	public static function searchPath($from, $to, $transportTypes = NULL) {

		if ($transportTypes === NULL) {
			$transportTypes = transportType::getTransportTypes();
		}

		$initial = new plannerState($from, NULL);
		$heuristic = function($state) use ($to) {
			$stop = $state->getStop();

			if ($stop == $to) {
				return 0; // Already there
			}

			if ($stop->getData("x") === NULL) {
				return 1; // Direct access
			}

			$x1 = $stop->getData("x");
			$x2 = $to->getData("x");
			$z1 = $stop->getData("z");
			$z2 = $to->getData("z");

			$deltaX = abs($x2 - $x1);
			$deltaZ = abs($z2 - $z1);

			return ($deltaX + $deltaZ) / 4;
		};
		$successors = function($node) use ($transportTypes) {
			$state = $node->getState();
			$stop = $state->getStop();
			$route = $state->getRoute();

			$result = array();

			$nexts = array();
			$stopGroup = $stop->getStopGroup();
			
			if ($stopGroup === NULL) {
				$nexts = $stop->getDirectConnections();
			} else {
				$nexts = $stopGroup->getDirectConnections($stop);
			}

			// Initial node
			if ($route === NULL) {
				foreach ($nexts as $next) {
					if (isset($next["passby"])) {
						$result[] = [
							"cost" => $next["time"],
							"state" => new plannerState($next["stop"], $next["route"], $next["passby"])
						];
					} else {
						$result[] = [
							"cost" => $next["time"],
							"state" => new plannerState($next["stop"], $next["route"])
						];
					}
				}
			} else {
				foreach ($nexts as $next) {
					$extraCost = 0;

					if ($next["route"] != $route) {
						$extraCost = self::$transferCost;
					}

					if (isset($next["passby"])) {
						$result[] = [
							"cost" => $next["time"] + $extraCost,
							"state" => new plannerState($next["stop"], $next["route"], $next["passby"])
						];
					} else {
						$result[] = [
							"cost" => $next["time"] + $extraCost,
							"state" => new plannerState($next["stop"], $next["route"])
						];
					}
				}
			}


			// Filter result to only use transports that are allowed

			$newResult = array();

			foreach ($result as $r) {
				$state = $r["state"];
				$route = $state->getRoute();
				$line = $route->getLine();
				$transportType = $line->getTransportType();

				if (!(in_array($transportType, $transportTypes))) {
					continue;
				}

				$newResult[] = $r;
			}

			return $newResult;
		};
		if ($to->getStopGroup() === NULL) {
			$goalTest = function($possibleEndNode) use ($to) {
				$stop = $possibleEndNode->getStop();
				return ($stop == $to);
			};
		} else {
			$goalTest = function($possibleEndNode) use ($to) {
				$stop = $possibleEndNode->getStop();
				return (in_array($stop, $to->getStopGroup()->getStops()));
			};
		}
		$astar = new astar($initial, $heuristic, $successors, $goalTest);
		$result = $astar->solve();

		return $result;

	}

	public static function nodeToListOfNodes($node) {
		$result = array();
		$size = $node->getDepth() + 1;

		for ($i = $size - 1; $i >= 0; $i--) {
			$result[$i] = $node;
			$node = $node->getParent();
		}

		return $result;
	}
}