<?php

require_once("init.php");

if (isset($_GET["page"])) {
	$page = $_GET["page"];
} else {
	$page = "index";
}

$page = str_replace("..", "", $page);
$pageDirectory = dirname(__FILE__) . "/pages/";
$realpath = $pageDirectory . $page . ".php";

if (substr($realpath, 0, strlen($pageDirectory)) != $pageDirectory) {
	error404();
}

if (file_exists($realpath)) {
	include($realpath);
} else {
	error404();
}