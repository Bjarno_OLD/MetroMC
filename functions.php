<?php

function error404() {
	http_response_code(404);
	die("404");
}

function head($title = "") {
global $loadTime;
?>
<!-- Loading data took <?= $loadTime; ?> ms -->
<!DOCTYPE html>
<html>
<head>
<title><?= $title; ?></title>
<style>
a {
	color: blue;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div id="menu">
<a href="?page=index">Index</a> | <a href="?page=lines">Lijnoverzicht</a> | <a href="?page=planner">Routeplanner</a> | <a href="?page=netmap">Netkaart</a>
</div>
<hr>
<?php
}

function foot() {
?>
</body>
</html>
<?php
}