<?php

namespace be\bjarno\astar;

/*
This is not entirely the standard A* implementation. It uses a few
workarounds to avoid recalculating stuff in order to get the cost of
a route.

Furthermore, there is a small adaptation that allows for graphs with more
than one edge between two nodes. It's quite dirty how this is handled here,
but it get's the job done at least.

The Priority Queue in PHP is being used, but it's not really flexible, a
better priority queue implementation should be used that allows rescheduling
in order to reschedule nodes with the same state.
*/

class node {
	private $state;
	private $parent;
	private $depth;
	private $cost;
	private $totalCost;
	private $data;

	public function __construct($parent, $state, $cost, $data = array()) {
		$this->parent = $parent;
		$this->state = $state;
		$this->cost = $cost;

		if ($parent !== NULL) {
			$this->totalCost = $parent->getTotalCost() + $cost;
			$this->depth = $parent->getDepth() + 1;
		} else {
			$this->totalCost = $cost;
			$this->depth = 0;
		}

		$this->data = $data;
	}

	public function getState() {
		return $this->state;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getDepth() {
		return $this->depth;
	}

	public function getCost() {
		return $this->cost;
	}

	public function getTotalCost() {
		return $this->totalCost;
	}

	public function getData($key = NULL, $default = NULL) {
		if ($key === NULL) {
			return $this->data;
		} else {
			if (isset($this->data[$key])) {
				return $this->data[$key];
			} else {
				return $default;
			}
		}
	}
}

class frontier extends \SplPriorityQueue {
	public function __construct() {
		$this->setExtractFlags(\SplPriorityQueue::EXTR_BOTH);
	}

	public function compare($p1, $p2) {
		if ($p1 == $p2) { return 0; }
		return $p1 > $p2 ? -1 : 1;
	}
}

class solver {

	private $initial; // State to begin with
	private $heuristic; // Function that computes the heuristic of a given state
	private $successors; // Function that returns a list of successors for a given node: should contain a list of arrays ["action" => ..., "state" => ..., "cost" => ...]
	private $goalTest; // Function that checks whether the result is a goal

	const DEBUG = false;

	public function __construct($initial, $heuristic, $successors, $goalTest) {
		$this->initial = $initial;
		$this->heuristic = $heuristic;
		$this->successors = $successors;
		$this->goalTest = $goalTest;
	}

	public function solve() {
		
		// Stack used for handling nodes with the same state
		$stack = array();

		// Switches with the real frontier/PQ to swap information to filter out nodes with the same state
		$tempPQ = new frontier();

		// The best solution (until now) is stored here
		$solution = NULL;

		// The frontier
		$frontier = new frontier();
		
		// The closed list (array is abused: list is kept in keys)
		// Source: https://lucb1e.com/?p=post&id=97
		$closedList = array();

		// Put the initial node on the PQ
		$rootHeuristic = call_user_func($this->heuristic, $this->initial);
		$root = new node(NULL, $this->initial, 0, $rootHeuristic, array("ROOT"));
		$frontier->insert($root, $rootHeuristic);

		while (!($frontier->isEmpty())) {

			$top = $frontier->top();
			$top = $top["data"];

			$frontier->next();

			if (self::DEBUG) {
				echo("Expanding " . $top->getState()->getUniqueId() . "...<br>");
				echo($top->getCost() . "/" . $top->getTotalCost() . "<br>");
			}

			if (call_user_func($this->goalTest, $top->getState())) {
				return $top;
			}

			$uid = $top->getState()->getUniqueId();

			if (!(isset($closedList[$uid]))) {
				$closedList[$uid] = true;
			}

			if (self::DEBUG) {
				echo("&nbsp;&nbsp;&nbsp;&nbsp;Closed list: ");
				$first = true;
				foreach ($closedList as $ele => $ign) {
					if (!($first)) {
						echo(" - ");
					} else {
						$first = false;
					}
					echo($ele);
				}
				echo("<br>");
			}

			$successors = call_user_func($this->successors, $top);

			foreach ($successors as $successor) {
				$state = $successor["state"];

				$uid = $state->getUniqueId();

				// Don't handle node if it is already processed
				if (!(isset($closedList[$uid]))) {
					$heuristic = (int) call_user_func($this->heuristic, $state);

					$data = (isset($successor["data"]) ? $successor["data"] : array());

					$newNode = new node($top, $state, $successor["cost"], $data);
					$priority = $newNode->getTotalCost() + $heuristic;
					$frontier->insert($newNode, $priority);

					if (self::DEBUG) {
						echo("&nbsp;&nbsp;&nbsp;&nbsp;Added " . $state->getUniqueId() . " (p: " . $newNode->getTotalCost() . " + " . $heuristic . " = " . $priority . ")<br>");
					}
				}
			}
			
		}

	}
}

interface state {
	public function getUniqueId();
}